<div align="center">
<h1>checksum4cj</h1>
</div>
<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v0.0.1-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.55.3-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjcov-100.0%25-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/state-孵化-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/domain-HOS/Cloud-brightgreen" style="display: inline-block;" />
</p>

##  1 介绍

### 1.1 项目特性

1. 本项目是使用仓颉实现的一个简单的计算散列函数的组件，如sha1，MD5等。
2. 本项目迁移自[checksum/checksum.js at master · dshaw/checksum (github.com)](https://github.com/dshaw/checksum/blob/master/checksum.js) 

### 1.2 项目计划

1. 计划对cangjie尚不支持的使用标准输入读取UTF-8 编码规则之外的字节，这一已知问题进行修复。

   ```cangjie
    ./main
    𠜎
   ```
   本项目中使用控制台输入"𠜎",会因为输入不是`UTF-8`编码，产生IllegalArgumentException异常。

2. 对本项目中可能潜在的bug进行不定期修复。

##  2 架构

### 2.1 项目结构

```shell
.
├── README.md
├── LICENSE
├── cjpm.toml
├── fixture
│   ├── 1px.gif
│   └── dshaw.txt
└── src
    ├── test
    │   └── checksum_test.cj    # 测试代码
    ├── checksum_cli.cj
    └── checksum.cj             # 核心代码
```

### 2.2 接口说明

| 名称                                                         | 用途                                                         |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| `public func checksum(data: String, algorithm: String = DEFAULT_ALGORITHM, encoding: String = DEFAULT_ENCODING): String`    | 计算给定字符串的哈希值。使用指定的摘要算法和编码方式。默认为 SHA256 算法和 Hex 编码。 |
| `public func checksum(data: Array<Byte>, algorithm!: String = DEFAULT_ALGORITHM, encoding!: String = DEFAULT_ENCODING): String` | 计算给定字节数组的哈希值。使用指定的摘要算法和编码方式。默认为 SHA256 算法和 Hex 编码。                         |
| `public func checksumFile(file: String, algorithm!: String = DEFAULT_ALGORITHM, encoding!: String = DEFAULT_ENCODING): String`| 计算给定文件的哈希值。使用指定的摘要算法和编码方式。默认为 SHA256 算法和 Hex 编码。如果文件不存在或路径无效，则抛出异常。                        |

## 3 使用说明

### 3.1 导入方式

在 `cjpm.toml` 中的 `[dependencies]` 内添加如下内容：

```toml
checksum4cj = { git = "https://gitcode.com/RainBoWli6/checksum4cj" }
```

添加后 `cjpm.toml` 如下所示：

```toml
[dependencies]
  checksum4cj = { git = "https://gitcode.com/RainBoWli6/checksum4cj" }
  
[package]
  cjc-version = "0.55.3"
  compile-option = ""
  description = "nothing here"
  link-option = ""
  name = "your_package_name_here"
  output-type = "executable"
  override-compile-option = ""
  src-dir = ""
  target-dir = ""
  version = "1.0.0"
  package-configuration = {}
```

### 3.2 编译构建（Win/Linux/Mac）

```shell
cjpm build
```

如果想使用 CLI 工具，需要使用以下命令进行编译：

```shell
cjc checksum.cj checksum_cli.cj
```

编译完成后，可以通过以下命令运行 CLI 工具：

```shell
./main
```

CLI 工具支持以下参数：

- `-a` 或 `--algorithm`: 指定要使用的哈希算法，默认是 `SHA256`。可选的算法包括 `MD5`, `SHA1`, `SHA224`, `SHA256`, `SHA384`, `SHA512`, `SM3`。  
  **示例**: 
  ```shell
  ./main -a sha1 ./file.txt
  ```

- `-e` 或 `--encoding`: 指定编码格式，默认为 `Hex`。可以选择其他编码格式，如 `Base64`。  
  **示例**: 
  ```shell
  ./main -e base64 ./file.txt
  ```

- `-v` 或 `--verbose`: 是否启用详细输出模式。此参数不需要额外的值，只需包含它即可启用详细模式。  
  **示例**: 
  ```shell
  ./main -v ./file.txt
  ```

### 3.3 功能示例
#### 3.3.1 计算给定字符串的哈希值,采用默认SHA256算法、默认Hex编码

```cangjie
import checksum4cj.*

main(): Int64 {

    let hash1 = checksum("dshaw")
    println(hash1)  //编码结果为"650a94547ad8ff00d5c5e3c8d54e8503d566b8ab0029dc289b5bb4294e39b92a"

    let hash2 = checksum("1234567890~!@#$%^&*()_+")
    println(hash2)  //编码结果为"cbfd8eb072b509a43c8171f735864e09e7df0ebf538d8c756d27891069c78f47"

    return 0
}
```

#### 3.3.2 计算给定文件的哈希值,采用默认SHA256算法、默认Hex编码

```cangjie
import checksum4cj.*

main(): Int64 {
    let hash1 = checksumFile("fixtures/dshaw.txt")
    println(hash1)  //编码结果为"650a94547ad8ff00d5c5e3c8d54e8503d566b8ab0029dc289b5bb4294e39b92a"

    let hash2 = checksumFile("fixtures/1px.gif")
    println(hash2)   //编码结果为"db7164038271366318dc041126f07aafe9bf671340966f4765948d0b07f6c5cc"

    return 0
}
```

#### 3.3.3 计算给定字符串的哈希值,采用默认SHA256算法、Base64编码

```cangjie
import checksum4cj.*

main(): Int64 {

    let hash1 = checksum("dshaw", encoding: "base64")
    println(hash1)  //编码结果为"ZQqUVHrY/wDVxePI1U6FA9VmuKsAKdwom1u0KU45uSo="

    let hash2 = checksum("1234567890~!@#$%^&*()_+", encoding: "base64")
    println(hash2)  //编码结果为"y/2OsHK1CaQ8gXH3NYZOCeffDr9TjYx1bSeJEGnHj0c="

    return 0
}
```

#### 3.3.4 计算给定文件的哈希值,采用默认SHA256算法、Base64编码

```cangjie
import checksum4cj.*

main(): Int64 {

    let hash1 = checksumFile("fixtures/dshaw.txt", encoding: "base64")
    println(hash1)  //编码结果为"ZQqUVHrY/wDVxePI1U6FA9VmuKsAKdwom1u0KU45uSo="

    let hash2 = checksumFile("fixtures/1px.gif", encoding: "base64")
    println(hash2)  //编码结果为"23FkA4JxNmMY3AQRJvB6r+m/ZxNAlm9HZZSNCwf2xcw="

    return 0
}
```


## 5参与贡献

本项目由 [SIGCANGJIE / 仓颉兴趣组](https://gitcode.com/SIGCANGJIE) 实现并维护。技术支持和意见反馈请提Issue。

本项目是仓颉兴趣组 [一星级里程碑项目](https://gitcode.com/SIGCANGJIE/homepage/wiki/CompentencyModel.md)。

本项目基于 MIT License，欢迎给我们提交PR，欢迎参与任何形式的贡献。

本项目committer：([RainBoWli6(@RainBoWli6) - GitCode](https://gitcode.com/RainBoWli6) )

This project is supervised by [@zhangyin-gitcode](https://gitcode.com/zhangyin_gitcode).